/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thidarat.shape;

/**
 *
 * @author User
 */
public class TestShape {

    public static void main(String[] args) {
        
        Cricle c1 = new Cricle(1.5);
        Cricle c2 = new Cricle(4.5);
        Cricle c3 = new Cricle(5.5);
        System.out.println(c1);
        System.out.println(c2);
        System.out.println(c3);
        System.out.println("-------------------");
        
        Rectangle r1 = new Rectangle(3, 2);
        Rectangle r2 = new Rectangle(4, 3);
        System.out.println(r1);
        System.out.println(r2);
        System.out.println("-------------------");
        
        Sqaure s1 = new Sqaure(2.0);
        Sqaure s2 = new Sqaure(4.0);
        System.out.println(s1);
        System.out.println(s2);
        System.out.println("-------------------");
        
        
        Shape[] shape = {c1, c2, c3, r1, r2, s1, s2};
        for (int i = 0; i < shape.length; i++) {
            System.out.println(shape[i].getName() + " area : " + shape[i].claArea());
        }
    }
}
