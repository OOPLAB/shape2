/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thidarat.shape;

/**
 *
 * @author User
 */
public class Cricle extends Shape { //Concrete calss

    private double r;
    private static final double PI = 22.0 / 7;

    public Cricle(double r) {
        super("Cricle");
        this.r = r;
    }

    public void setR(double r) {
        this.r = r;
    }

    @Override
    public double claArea() {
        return PI * r * r;
    }

    @Override
    public String toString() {
        return "Cricle{" + "r=" + r + '}';
    }

}
